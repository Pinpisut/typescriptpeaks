import styled from '@emotion/styled';
import color from '@constants/color';

export const Styled = styled.div`
  label: footer;
  width: 100vw;
  background: ${color['primary-color']};
  height: 100px;
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;

  a:visited {
    color: ${color['text-color-secondary']};
  }
`;

export const RefText = styled.h4`
  color: ${color['text-color-secondary']}
`;

export default {};