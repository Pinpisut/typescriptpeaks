import React from 'react';
import { Styled, RefText } from './Footer.style';

const Footer: React.FC = () => {
  return (
    <Styled>
      <RefText>Font made from <a href="http://www.onlinewebfonts.com">{' oNline Web Fonts '}</a>is licensed by CC BY 3.0</RefText>
    </Styled>
  );
};

export default Footer;
