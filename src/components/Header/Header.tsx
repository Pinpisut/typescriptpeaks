/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
import React, { useState, useRef } from 'react';
import { useHistory } from 'react-router-dom'; 

import { getImages } from '../../utils';

import { 
  Styled, 
  ImageDiv,
   SearchDiv, 
   SearchItem, 
   SearchIcon 
} from './Header.style';

/**
 * ref event : https://stackoverflow.com/questions/56729530/typescript-how-to-access-onchange-event-data-without-error-object-is-possibly
 * ref event : https://stackoverflow.com/questions/44321326/property-value-does-not-exist-on-type-eventtarget-in-typescript
 * ref useRef with hook : https://www.designcise.com/web/tutorial/how-to-fix-object-is-possibly-null-typescript-error-when-using-useref-react-hook
 */

const Header: React.FC = () => {
  const [searchValue, setSearchValue] = useState('');
  const inputRef = useRef<HTMLInputElement>(null);
  const history = useHistory();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const key = event.target.value;
    setSearchValue(key);
    if (key !== '') {
      history.push(`/search/${key}`);
    } else {
      history.push('/search/noneofdata');
    }
  };

  const goBackHome = () => {
    history.push('/');
    handleClearValue();
  };

  const handleClearValue = () => {
    setSearchValue('');
  };

  const handleFocus = () => {
    inputRef?.current?.focus();
  };

  return (
    <Styled>
      <ImageDiv src={getImages('Logo-white-blue.png')} alt="header-img" onClick={goBackHome} />
      <SearchDiv onClick={handleClearValue}>
        <SearchItem ref={inputRef} type="text" value={searchValue} placeholder="Search all news" onChange={handleChange} />
        {/* <input type="text" placeholder="Enter e-mail" ref={inputRef} /> */}
        <SearchIcon src={getImages('search-icon.svg')} alt="search-icon" onClick={handleFocus} />
      </SearchDiv>
    </Styled>
  );
};

export default Header;
