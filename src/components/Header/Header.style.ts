import styled from '@emotion/styled';
import color from '@constants/color';

import { mediaQuery } from '../../utils';

export const Styled = styled.div`
  label: header;
  width: 100vw;
  background: ${color['primary-color']};
  padding: 0 12%;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: initial;
  z-index: 1;

  ${mediaQuery(
    'md',
    `
      flex-direction: column;
      align-items: center;
  `
  )};
`;

export const ImageDiv = styled.img`
  width: 133px;
  height: 50px;
  margin: 35px 0px;
`;

export const SearchItem = styled.input`
  width: 70px;
  height: 30px;
  background: ${color['primary-color']};
  position: absolute;
  bottom: 0;
  transform-origin: 100% 50%;
  transition: all ease 0.5s;
  border: none;
  border-bottom: 2px solid ${color['secondary-color']};

  &::placeholder {
    color: transparent;
  }

  &:focus {
    outline: 0;
    background: ${color['bg-search-scale']};
    transform: translateX(-70px);
    width: 210px;
    border: none;
    border-bottom: 2px solid ${color['secondary-color']};
    padding-left: 5px;
    color: ${color['search-text-color']};
  }

  &:not(:focus){
    color: transparent;
  }

  &:focus::placeholder {
    color: ${color['search-text-color']};
  }

  ${mediaQuery(
    'md',
    `
      transform-origin: 50% 50%;
      transform: translateX(0px);
      width: 140px;

      &:focus {
        transform-origin: 50% 50%;
        transform: translateX(0px);
      }

      &:focus::placeholder {
        text-align: left;
      }

      &::placeholder {
        color: ${color['search-text-color']};
        text-align: center;
      }

      &:not(:focus){
        color: ${color['search-text-color']};
        text-align: center;
      }
  `
  )};
`;

export const SearchDiv = styled.div`
  width: 70px;
  position: relative;
  display: flex;
  justify-content: center;

  ${mediaQuery(
    'md',
    `
      margin-top: 50px;
  `
  )};
`;

export const SearchIcon = styled.img`
    width: 14px;
    height: auto;
    position: absolute;
    bottom: 8px;
    right: 45%;

    ${mediaQuery(
      'md',
      `
        display: none;
    `
    )};
`;

export default {};