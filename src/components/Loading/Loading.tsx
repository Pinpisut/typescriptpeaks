import React from 'react';
import { Styled, LoadDiv } from './Loading.style';

const Loading: React.FC = () => {
  return (
    <Styled>
      <LoadDiv />
    </Styled>
  );
};

export default Loading;
