import styled from '@emotion/styled';
import color from '@constants/color';

export const Styled = styled.div`
  label: loading;
  width: 100%;
  margin-top: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LoadDiv = styled.div`
  width: 80px;
  height: 80px;

  animation: circle infinite .75s linear;

  border: 7px solid ${color['primary-color']};
  border-top-color: transparent;
  border-radius: 100%;

  @keyframes circle
  {
    0%
    {
        transform: rotate(0);
    }
    100%
    {
        transform: rotate(360deg);
    }
  }
`;

export default {};
