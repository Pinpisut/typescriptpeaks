/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
import React from 'react';
import { useHistory } from 'react-router-dom';

import { getImages } from '../../utils';

import { 
  Styled, 
  BgImage, 
  TextDiv, 
  TextTitle, 
  TextBody 
} from './NewsCard.style';

interface NewsCardProps {
  className: string,
  title: string,
  onClick?(select:string): unknown
  pathImg: string,
  lineColor: string,
  body: string,
  path: string
}

const NewsCard: React.FC<NewsCardProps> = ({ className, title, body, path, pathImg, lineColor }) => {
  const checkForRemoveTop = (className === 'sub-higlight' ||  pathImg !== '');
  const addMorePaddingTop = (pathImg !== '');
  const isOnlyTextPattern = (className !== 'normal' && className !== 'highlight' && pathImg === '');

  const history = useHistory();

  // route to other
  const handleClick = () => {
    history.push(path);
  };

  return (
    <Styled
      className={className}
      onClick={handleClick}
      lineColor={lineColor}
      shouldAddPaddingTop={addMorePaddingTop}
    >
      <BgImage haveImage={checkForRemoveTop} mdHaveImage={pathImg !== ''} src={pathImg || getImages('image-default-500.png')} alt="card-img" />
      <TextDiv isOnlyTextPattern={isOnlyTextPattern}>
        <TextTitle className="font-serif">{title}</TextTitle>
        {body !== '' && <TextBody className="font-opensan-serif">{body}</TextBody>}
      </TextDiv>
    </Styled>
  );
};

export default NewsCard;
