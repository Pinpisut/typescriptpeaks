import styled from '@emotion/styled';
import color from '@constants/color';

import { mediaQuery } from '../../utils';

/**
 * ref hex color : https://github.com/microsoft/TypeScript/issues/6579 
 */

// type Hex = (n:string) => n.match(/^([0-9]|[A-F])+$/)
// type CssColor = /^#([0-9a-f]{3}|[0-9a-f]{6})$/i;
type StyledProps = {
    lineColor: string,
    shouldAddPaddingTop: boolean
}

export const Styled = styled.div<StyledProps>`
  width: 100%;
  height: 100%;
  background: ${color['bg-logo-no-image']};
  position: relative;
  border-bottom: 3px solid ${props => props.lineColor};
  padding-top: ${props => (props.shouldAddPaddingTop)? '100%' : '0px'};
  box-shadow: 1px 1px 5px ${color['text-color-thrid']};

  &.normal {
    padding-top: 100%;

    ${mediaQuery('md', `
      padding-top: 0;
    `)}
  }
`;

type TextDivProps = {
    isOnlyTextPattern: boolean
}

export const TextDiv = styled.div<TextDivProps>`
  width: 100%;
  height: ${props => props.isOnlyTextPattern ? '100%' : '120px'};
  position: absolute;
  bottom: 0;
  left:0;
  background: ${props => props.isOnlyTextPattern ? `rgba(${color['primary-color-rgb']},1)` : `rgba(${color['primary-color-rgb']},0.9)`};
  color: ${color['secondary-color']};
  font-size: 14px;
  padding: 8px;
  transition: min-height 0.5s ease-in;
  display: flex;
  flex-direction: column;

  clip-path: inset(0px 0px 0px 0px);

  &:hover {
    height: 100%;
    transition: height 0.5s ease-out;
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
    padding: 20px;
    text-align: center;
    background: rgba(${color['primary-color-rgb']},0.9);
  }

  ${mediaQuery(
    'lg',
    `
    height: 80px;
  `
  )};

  ${mediaQuery(
    'md',
    `
    height: 60px;
  `
  )};

`;

export const TextTitle = styled.h3`
  
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;  
  overflow: hidden;

  line-height: 1;
`;

export const TextBody = styled.h4`
  margin-top: 5px;
  display: flex;

  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;  
  overflow: hidden;

  ${mediaQuery(
    'xl',
    `
    display: none;
  `
  )};
`;

type BgImageProps = {
    haveImage: boolean,
    mdHaveImage: boolean
}

export const BgImage = styled.img<BgImageProps>`
  width: 100%;
  height: 100%;
  object-fit: cover;
  position: absolute;
  top: ${props => props.haveImage ? '0' : '-30'}px;
  clip-path: ${props => props.haveImage ? '' : 'inset(30px 0px 0px 0px)'};
  left: 0;

  ${props => {
    return `
      ${mediaQuery('md', `
        top: ${props.mdHaveImage ? '0' : '-30'}px;
        clip-path: ${props.mdHaveImage ? '' : 'inset(30px 0px 0px 0px)'};
      `)}
    `;
  }}
`;

export default {};