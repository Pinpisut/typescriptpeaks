import React from 'react';
import { Styled } from './ErrorMessage.style';

interface ErrorMessageProps {
  text: string
}

const ErrorMessage: React.FC<ErrorMessageProps> = ({ text }) => {
  return (
    <Styled>
      <h3 className="font-opensan-light">{text}</h3>
    </Styled>
  );
};

export default ErrorMessage;
