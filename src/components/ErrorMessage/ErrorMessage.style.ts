import styled from '@emotion/styled';
import color from '@constants/color';

export const Styled = styled.div`
  label: errormessage;
  width: 100%;
  color: ${color['text-color']};
  height: 100px;
  margin-top: 100px;
  text-align: center;
`;
