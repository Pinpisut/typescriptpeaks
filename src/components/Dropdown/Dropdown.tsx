/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
import React, { useState } from 'react';
import { map, find, get } from 'lodash';

import { getImages } from '../../utils';
import { 
  Styled, 
  ControllerBtn, 
  ItemDropdown, 
  TextTitle, 
  ImageDiv,
  ContainerMenuDetail,

} from './Dropdown.style';

const dataDropdown = [
  {
    title: 'Newest First',
    key: 'newsest',
  },
  {
    title: 'Oldest First',
    key: 'oldest',
  }
];

interface DropdownProps {
  onClick(select:string): unknown
}

const Dropdown: React.FC<DropdownProps> = ({ onClick }) => {
  const [showDrop, setshowDrop] = useState(false);
  const [nowSelect, setSelected] = useState('newsest');
  const [nowTitle, setTitle] = useState('Newest First');

  const showMenu = () => {
    setshowDrop(!showDrop);
  };

  const changeNowSelect = (select:string) => {
    setSelected(select);
    const selectTitle = find(dataDropdown, obj => obj.key === select);
    setTitle(get(selectTitle, 'title', '-'));
    setshowDrop(false);

    onClick(select);
  };

  const generateMenuHam = () => {
    return map(dataDropdown, (each, index) => (
      <div key={`itemdropdown-${each.key}`} className="eachitem" onClick={() => changeNowSelect(each.key)}>
        <ItemDropdown className="normal" index={index} showMenu={showDrop}>
          <TextTitle className="font-san-serif">{get(each, 'title')}</TextTitle>
          {index === 0 && <ImageDiv className="up" src={getImages('arrowdropdown.png')} alt="arrowdropdown-img" />}
        </ItemDropdown>
      </div>  
    ));
  };

  return (
    <Styled>
      <ControllerBtn className="controlbtn" onClick={() => showMenu()}>
        <ItemDropdown>
          <TextTitle className="font-san-serif">{nowTitle}</TextTitle>
          <ImageDiv src={getImages('arrowdropdown.png')} alt="arrowdropdown-img" />
        </ItemDropdown>
      </ControllerBtn>
      {showDrop &&
        <ContainerMenuDetail className="menu-detail">
          <div className="container-subitem">
            {generateMenuHam()}
          </div>
        </ContainerMenuDetail>
      }
    </Styled>
  );
};

export default Dropdown;
