import styled from '@emotion/styled';
import color from '@constants/color';
import { widthSize, heightSize } from '@constants/staticVariable';

import { mediaQuery } from '../../utils';

export const Styled = styled.div`
  label: dropdown;
  width: ${widthSize}px;
  background: transparent;
  height: 30px;
  margin-top: 5px;
  margin-left: 20px;
  position: relative;
  z-index: 1;

  ${mediaQuery('xl', `
    width: ${widthSize - 100}px;
  `)}

  ${mediaQuery('md', `
    align-items: center;
    margin: 20px 0;
  `)}
`;

export const ControllerBtn = styled.div``;

export const ContainerMenuDetail = styled.div`
  label: container-menuHam-detail;
  background: ${color['secondary-color']};
  width: ${widthSize}px;
  position: absolute;
  top: 0;
  border: 1px solid ${color['line-color']};

  ${mediaQuery('xl', `
    width: ${widthSize - 100}px;
  `)}
`;

type ItemDropdownProps = {
    index?: number
    showMenu?: boolean
}

export const ItemDropdown = styled.div<ItemDropdownProps>`
  width: ${widthSize}px;
  height: ${heightSize}px;
  background: ${color['secondary-color']};
  border-bottom: 1px solid ${color['line-color']};
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 5px;

  &.normal {
    width: ${widthSize - 2}px;
    background: ${color['secondary-color']};
    border-bottom: 1px solid transparent;

    ${mediaQuery('xl', `
      width: ${widthSize - 102}px;
    `)}
  }

  ${props => {
    return `
      position: 'absolute';
      top: ${heightSize * props.index};
      width: ${(props.showMenu) ? widthSize - 2 : widthSize}px;
    `;
  }}

  ${mediaQuery('xl', `
    width: ${widthSize - 100}px;
  `)}
`;

export const ImageDiv = styled.img`
  width: 18px;
  height: 18px;

  &.up {
    transform-origin: 50% 50%;
    transform: rotate(180deg);
  }
`;

export const TextTitle = styled.h4`
  font-size: 14px;
  font-weight: bold;
`;

export default {};