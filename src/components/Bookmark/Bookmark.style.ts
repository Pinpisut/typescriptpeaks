import styled from '@emotion/styled';
import color from '@constants/color';

export const Styled = styled.div`
  label: bookmark;
  width: 160px;
  background: ${color['primary-color']};
  height: 30px;
  border-radius: 5px;
  margin-top: 5px;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;

export const TextTitle = styled.h4`
  font-weight: bold;
  color: ${color['text-color-secondary']};
`;

export const ImageDiv = styled.img`
  width: 12px;
  height: auto;
`;
