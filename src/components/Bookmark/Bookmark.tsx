/* eslint-disable complexity */
import React, { useState } from 'react';
// import PropTypes, { InferProps } from 'prop-types';

import { getImages } from '../../utils';
import { Styled, TextTitle, ImageDiv } from './Bookmark.style';


interface BookmarkProps {
  children?: React.ReactNode;
  nowBook: boolean,
  nowArticle?: boolean,
  onClick(isBook: boolean): unknown; // using unknow instand of any
}

const Bookmark: React.FC<BookmarkProps> = ({ nowBook, nowArticle, onClick }) => {
  const [ isBook ] = useState(nowBook || false);
  const titleOnArticlePage = isBook ? 'REMOVE BOOKMARK' : 'ADD BOOKMARK';
  const title = (!nowArticle) ? 'VIEW BOOKMARK' : titleOnArticlePage;

  return (
    <Styled onClick={() => onClick(isBook)}>
      {isBook && <ImageDiv src={getImages('bookmarkon-icon.svg')} alt="bookmarkon" />} 
      {!isBook && <ImageDiv src={getImages('bookmarkon-icon.svg')} alt="bookmarkoff" />}
      <TextTitle className="font-san-serif">{title}</TextTitle>
    </Styled>
  );
};

export default Bookmark;
