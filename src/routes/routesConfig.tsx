import { RouteConfig } from 'react-router-config';
import MainLayout from '@layouts/MainLayout';
import HomePage from '@pages/HomePage';
import ArticlePage from '@pages/ArticlePage';
import SearchPage from '@pages/SearchPage';
import SavedArticlePage from '@pages/SavedArticlePage';
import RouteNotFound from './RouteNotFound';

const routes: RouteConfig[] = [
  {
    path: '/',
    exact: true,
    component: MainLayout,
    routes: [
      {
        path: '/',
        exact: true,
        component: HomePage,
      },
      {
        component: RouteNotFound,
      },
    ],
  },
  {
    path: '/article/:frompage/:id',
    exact: true,
    component: ArticlePage,
  },
  {
    path: '/savearticle',
    exact: true,
    component: SavedArticlePage,
  },
  {
    path: '/search/:key',
    exact: true,
    component: SearchPage,
  },
  {
    component: RouteNotFound,
  },
];

export default routes;
