import React from 'react';
import { Redirect } from 'react-router-dom';

const RouteNotFound: React.FC = () => <Redirect to={{ ...location, state: { isNotFoundPage: true } }} />;

export default RouteNotFound;
