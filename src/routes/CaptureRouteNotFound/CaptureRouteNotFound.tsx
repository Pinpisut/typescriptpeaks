import * as React from 'react';
import { useLocation } from 'react-router-dom';
import NotFoundPage from '@pages/NotFoundPage';

const CaptureRouteNotFound: React.FC = ({ children }) => {
  const { state } = useLocation<{ isNotFoundPage: boolean }>();
  return state?.isNotFoundPage ? <NotFoundPage /> : <>{children}</>;
};

export default CaptureRouteNotFound;
