import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import renderWithRouter from '@helpers/tests/renderWithRouter';
import renderWithTheme from '@helpers/tests/renderWithTheme';
import CaptureRouteNotFound from './CaptureRouteNotFound';

describe('CaptureRouteNotFound', () => {
  it('should renders children by default ', () => {
    const { getByText } = renderWithRouter(
      <CaptureRouteNotFound>
        <h1>I am a child</h1>
      </CaptureRouteNotFound>,
    );
    expect(getByText('I am a child')).toBeInTheDocument();
  });

  it('should not show children if isNotFoundPage is true in location state', () => {
    const { queryByText } = renderWithTheme(
      <MemoryRouter initialEntries={[{ pathname: '404', state: { isNotFoundPage: true } }]}>
        <CaptureRouteNotFound>
          <h1>I am a child</h1>
        </CaptureRouteNotFound>
      </MemoryRouter>,
    );
    expect(queryByText('I am a child')).not.toBeInTheDocument();
  });
});
