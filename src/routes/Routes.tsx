import React from 'react';
import { renderRoutes } from 'react-router-config';
import CaptureRouteNotFound from './CaptureRouteNotFound';
import routesConfig from './routesConfig';

const Routes: React.FC = () => <CaptureRouteNotFound>{renderRoutes(routesConfig)}</CaptureRouteNotFound>;

export default Routes;
