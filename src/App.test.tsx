import * as React from 'react';
import App from './App';
import renderWithTheme from '@helpers/tests/renderWithTheme';

describe('<App>', () => {
  it('should render homepage by default', () => {
    const { getByText } = renderWithTheme(<App />);
    expect(getByText('Home Page')).toBeInTheDocument();
  });
});
