import axios from 'axios';

const baseUrl = 'https://content.guardianapis.com';
const apiKey = '0135e283-1cf9-45bf-93d0-a1675b178c98';
const showBlocks = 'all';

// https://content.guardianapis.com/news?show-blocks=all&api-key=0135e283-1cf9-45bf-93d0-a1675b178c98

const api = axios.create({});

const API = ({section = 'news'}) => {
  return new Promise(async (resolve, reject) => {
    try {
      const urlAndParam = `${baseUrl}/${section}?show-blocks=${showBlocks}&api-key=${apiKey}`;
      const response = await api.get(urlAndParam);
      
      resolve(response.data);
    } catch (error) {
      console.log('fetch error :', {section, error: error});
      console.log('see more error : ', error.response.data);
    }
  });
};

// export const getNewFromSection = (section) => {
//   const setting = { section };
//   const response = API(setting);
//   return response;
// };
