import React from 'react';
import { renderRoutes, RouteConfigComponentProps } from 'react-router-config';

const MainLayout: React.FC<RouteConfigComponentProps> = ({ route }) => <>{renderRoutes(route?.routes)}</>;

export default MainLayout;
