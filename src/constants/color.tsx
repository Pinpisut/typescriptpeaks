const color = {
    'primary-color': '#173577',       // blue
    'primary-color-rgb': '23,53,119', // blue rgb
    'secondary-color': '#fff',        
    'text-color': '#212121',          // nearly black
    'text-color-secondary': '#fff',   
    'text-color-thrid': '#909090',    // gray : under images on articlepage 
    'line-color': '#eaeaea',          // light gray
    'line-color-rgb': '234,234,234',  // light rgb
    'line-green': '#3a923a',
    'line-red': '#dc2e2c',
    'line-yellow': '#ffc000',
    'line-lightblue': '#2c9af1',      
    'bg-search-scale': '#2153a3',     // bg light blue when click magnify glass button
    'bg-logo-no-image': '#2a3e9f',    // bg blue : for default logo'image (newsCard)
    'search-text-color': '#7997c7'    // light blue for search' button
  };
  
export default color;