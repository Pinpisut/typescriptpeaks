import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import StylesAndThemeProvider from '@providers/StylesAndThemeProvider';
import Routes from '@routes/Routes';

import { Header, Footer } from '@components/index';

const App: React.FC = () => {
  return (
    <StylesAndThemeProvider>
      <BrowserRouter>
        <Header />
          <Routes />
        <Footer />
      </BrowserRouter>
    </StylesAndThemeProvider>
  );
};

export default App;
