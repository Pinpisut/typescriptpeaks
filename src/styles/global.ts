import { injectGlobal } from '@emotion/css';
import { mediaQuery } from '../utils';

injectGlobal`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -webkit-touch-callout: none; /* iOS Safari */
      -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
          -moz-user-select: none; /* Firefox */
          -ms-user-select: none; /* Internet Explorer/Edge */
              user-select: none; /* Non-prefixed version, currently
                                    supported by Chrome and Opera */
  }
  ::-webkit-scrollbar {
    width: 0px;  /* Remove scrollbar space */
    background: transparent;  /* Optional : just make scrollbar invisible */
  }
  ::-webkit-scrollbar-thumb {
    background: transparent;
  }
  html {
    font-size: 10px;
    font-weight: normal;
  }
  body {
    margin: 0;
    padding: 0;
    font-family: 'Open Sans';
  }
  a {
    color: white;
    cursor: default;
    text-decoration: none;
  }
  .switch-wrapper {
    position: relative;
    width: 100%;
    height: 100%;
  }
  .switch-wrapper > div {
    position: absolute;
    width: 100%;
    height: 100%;
  }
  .switch-wrapper > div > div {
    width: 100%;
    height: 100%;
  }
  h1 {
    font-size: 55px;

    ${mediaQuery(
      'lg',
      `
      font-size: 40px;
    `
    )};
  }
  h2 {
    font-size: 40px;

    ${mediaQuery(
      'lg',
      `
      font-size: 30px;
    `
    )};
  }
  h3 {
    font-size: 22px;

    ${mediaQuery(
      'lg',
      `
      font-size: 16px;
    `
    )};
  }
  h4 {
    font-size: 13px;

    ${mediaQuery(
      'lg',
      `
      font-size: 10px;
    `
    )};
  }
  h5 {
    font-size: 12px;

    ${mediaQuery(
      'lg',
      `
      font-size: 10px;
    `
    )};
  }
  .font-opensan-serif {
    font-family: "Open Sans", open-sans, sans-serif;
  }
  .font-san-serif {
    font-family: "Maison Neue Light", open-sans, sans-serif;
  }
  .font-serif {
    font-family: "UTM Times", serif;
  }
  .font-opensan-light {
    font-family: "Open Sans Light", open-sans, sans-serif;
  }
  .mrt-20 {
    margin-top: 20px;
  }
`;