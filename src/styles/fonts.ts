import { injectGlobal } from '@emotion/css';

/**
 * All font from https://www.onlinewebfonts.com/ is licensed by CC BY 3.0
 */

injectGlobal`

  @font-face {font-family: "Maison Neue Light";
    src: url("//db.onlinewebfonts.com/t/8b59d4b47037b08d452d8cb659cc2d53.eot");
    src: url("//db.onlinewebfonts.com/t/8b59d4b47037b08d452d8cb659cc2d53.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/8b59d4b47037b08d452d8cb659cc2d53.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/8b59d4b47037b08d452d8cb659cc2d53.woff") format("woff"), url("//db.onlinewebfonts.com/t/8b59d4b47037b08d452d8cb659cc2d53.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/8b59d4b47037b08d452d8cb659cc2d53.svg#Maison Neue Light") format("svg");
  }

  @font-face {
    font-family: "Open Sans";
    src: url("//db.onlinewebfonts.com/t/629a55a7e793da068dc580d184cc0e31.eot");
    src: url("//db.onlinewebfonts.com/t/629a55a7e793da068dc580d184cc0e31.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/629a55a7e793da068dc580d184cc0e31.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/629a55a7e793da068dc580d184cc0e31.woff") format("woff"), url("//db.onlinewebfonts.com/t/629a55a7e793da068dc580d184cc0e31.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/629a55a7e793da068dc580d184cc0e31.svg#Open Sans") format("svg"); 
  }

  @font-face {font-family: "UTM Times";
    src: url("//db.onlinewebfonts.com/t/0c621f7d2af19e6955cef821ee304a16.eot");
    src: url("//db.onlinewebfonts.com/t/0c621f7d2af19e6955cef821ee304a16.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/0c621f7d2af19e6955cef821ee304a16.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/0c621f7d2af19e6955cef821ee304a16.woff") format("woff"), url("//db.onlinewebfonts.com/t/0c621f7d2af19e6955cef821ee304a16.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/0c621f7d2af19e6955cef821ee304a16.svg#UTM Times") format("svg");
  }

  @font-face {
    font-family: "Open Sans Light";
    src: url("//db.onlinewebfonts.com/t/1bf71be111189e76987a4bb9b3115cb7.eot");
    src: url("//db.onlinewebfonts.com/t/1bf71be111189e76987a4bb9b3115cb7.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/1bf71be111189e76987a4bb9b3115cb7.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/1bf71be111189e76987a4bb9b3115cb7.woff") format("woff"), url("//db.onlinewebfonts.com/t/1bf71be111189e76987a4bb9b3115cb7.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/1bf71be111189e76987a4bb9b3115cb7.svg#Open Sans Light") format("svg");
  }
`;