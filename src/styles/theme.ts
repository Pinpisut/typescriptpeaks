import { SerializedStyles } from '@emotion/react';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme();

export type CustomizedTheme = typeof theme;

export type StyleFunction = (theme: CustomizedTheme) => SerializedStyles;

declare module '@emotion/react' {
  export interface Theme extends CustomizedTheme {}
}

export default theme;
