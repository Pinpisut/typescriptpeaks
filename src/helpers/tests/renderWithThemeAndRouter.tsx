import React from 'react';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import StylesAndThemeProvider from '@providers/StylesAndThemeProvider';

function renderWithThemeAndRouter(ui: React.ReactElement, options?: Omit<RenderOptions, 'queries'>): RenderResult {
  return render(
    <StylesAndThemeProvider>
      <MemoryRouter>{ui}</MemoryRouter>
    </StylesAndThemeProvider>,
    options,
  );
}

export default renderWithThemeAndRouter;
