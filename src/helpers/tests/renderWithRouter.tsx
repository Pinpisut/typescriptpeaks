import React from 'react';
import { render, RenderOptions, RenderResult } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

function renderWithRouter(ui: React.ReactElement, options?: Omit<RenderOptions, 'queries'>): RenderResult {
  return render(<MemoryRouter>{ui}</MemoryRouter>, options);
}

export default renderWithRouter;
