import React from 'react';
import { render, RenderOptions, RenderResult } from '@testing-library/react';

import StylesAndThemeProvider from '@providers/StylesAndThemeProvider';

function renderWithTheme(ui: React.ReactElement, options?: Omit<RenderOptions, 'queries'>): RenderResult {
  return render(<StylesAndThemeProvider>{ui}</StylesAndThemeProvider>, options);
}

export default renderWithTheme;
