// created by Pinpisut Sengcha
import React, {useState} from 'react';
import { 
    get,
    filter,
    find,
    map,
    sortBy,
    reverse,
    reduce,
    split,
    join,
    size,
    includes,
    toLower
  } from 'lodash';
  
  const { NODE_ENV } = process.env;
  
  // mediaQuery
  type breakpointsOption = {
    [key: string]: string
  }
  export const breakpoints:breakpointsOption = {
    xs: '320px',
    md: '767px',
    lg: '991px',
    xl: '1199px'
  };

  // without arrow function
  // export function mediaQuery(deviceSize: string, value: string): string {
  //   const screenSize = breakpoints[deviceSize] || '0px';
  //   return `@media (max-width: ${screenSize}) {
  //       ${value}
  //   }`;
  // }

  export const mediaQuery = (deviceSize: string, value: string): string => {
    const screenSize = breakpoints[deviceSize] || '0px';
    return `@media (max-width: ${screenSize}) {
        ${value}
    }`;
  };
  
  // get image'file path
  
  export const getFile = (path:string):string => NODE_ENV === 'development' ? `/${path}` : `./${path}`;
  export const getImages = (fileName:string):string => getFile(`images/${fileName}`);
  
  // formatting data for ready use
  type objectOption = {
    [key: string]: string[]
  }
  // const getBodyData = (body:getDataOption): getDataOption => {
  //   const [ first ] = body;
  //   const dBodyHtml = get(first, 'bodyHtml', '');
  //   const dBodyText = get(first, 'bodyTextSummary', '');
  //   const dBodyExample = (dBodyText !== '') ? `${dBodyText.substring(0, 80)}...` : '';
  //   const dArrayBodyText = split(dBodyText, '.');
  //   const dJoinArrayBodyText = join(dArrayBodyText, `.<br /><br />`);
  //   const dElements = get(first, 'elements', []);
  //   const filterImage = filter(dElements, item => item.type === 'image');
  //   return ({

  //   })
  // }

  /**
   * // getBodyData(dBody);
    // const allImage = reduce(filterImage, (acc, obj, index) => {
    //   const eachAssets = get(obj, 'assets');
    //   const caption = get(obj,'imageTypeData.caption', '');
    //   const addMoreCaption = {
    //     [`asset`] : eachAssets,
    //     [`caption`] : caption
    //   }
    //   return [
    //     ...acc,
    //     addMoreCaption,
    //   ]
    // }, [])
    // const assetData = get(allImage, '0.asset');
    // const dCaption = get(allImage, '0.caption', '');
    // const data1000 = find(assetData, element => element.typeData.width === 1000) || {};
    // const getUrl1000 = get(data1000, 'file', getImages('image-default-1000.png'));
    // const data500 = find(assetData, element => element.typeData.width === 500) || {};
    // const getUrl500 = get(data500, 'file', '');
    // let defaultImage = '';
    // if (get(data1000, 'file') !== undefined) {
    //   defaultImage = '1000';
    // } else if (get(data500, 'file') !== undefined) {
    //   defaultImage = '500';
    // } else {
    //   defaultImage = 'none';
    // }
   */

  
  // export const getFormattingData = (object:objectOption):objectOption => {
  //   const renewId = join(split(get(object, 'id', ''), '/'), '-');
  //   const dId = renewId;
  //   const sectionType = toLower(get(object, 'sectionName'), '');
  //   const dTitle = get(object, 'webTitle', '');
  //   const dDate = get(object, 'webPublicationDate', '');
  //   const dHeadline = get(object, 'headline', '');
    
  //   // get body
  //   // const dBody = get(object, 'blocks.body', []);
  
  //   return ({
  //     id: dId,
  //     sectionType: sectionType,
  //     title: dTitle, // webTitle
  //     date: dDate, // webPublicationDate
  //     headline: dHeadline, // headline
  //     // bodyHtml: dBodyHtml, // bodyHtml
  //     // bodyExample: dBodyExample,
  //     // bodyText: dJoinArrayBodyText, // bodyTextSummary
  //     // image: getUrl1000, // elements.image.assets "width":1000,
  //     // imageThumb: getUrl500, // elements.image.assets "width":500
  //     // defaultImage: defaultImage,
  //     // caption: dCaption
  //   });
  // };
  
  // sort newsCard
  // Array<string> === string[]
  type sortItemOption = {
    [key: string]: string
  };
  export const sortNewest = (arr:Array<sortItemOption>):Array<sortItemOption> => {
    // const dateData = (item:sortOption):string[] => get(item, 'date'); // not use
    const datasort = sortBy(arr, (item:sortItemOption) => new Date( get(item, 'date') ));
    // return reverse(datasort);
    return reverse(datasort);
  };
  
  export const sortOldest = (arr:Array<sortItemOption>):Array<sortItemOption> => {
    const datasort = sortBy(arr, (item:sortItemOption) => new Date( get(item, 'date') ));
    // return datasort;
    return datasort;
  };
  
  // find using image : if path of size(1000) is not available, using size(500) 
  type findUsingImageOption = {
    [key: string]: string
  }
  export const findUsingImage = (data:findUsingImageOption):string => {
    if (get(data, 'defaultImage') === '1000') {
      return get(data, 'image');
    }
    return get(data, 'imageThumb');
  };
  
  // search data with key
  type searchByKeyOption = {
    [key: string]: string
  };
  export const searchByKey = (data:Array<searchByKeyOption> , key:string): Array<searchByKeyOption> => {
    const lowerKey = toLower(key);
    const searchData:Array<searchByKeyOption> = filter(data, item => {
      const titleText = toLower(get(item, 'title'));
      const sectionText = get(item, 'sectionType');
  
      return includes(titleText, lowerKey) || includes(sectionText, lowerKey);
    });
    return searchData;
  };

  export default {};
  