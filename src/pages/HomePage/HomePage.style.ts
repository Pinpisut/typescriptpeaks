// import { css } from '@emotion/react';
import styled from '@emotion/styled';

import color from '@constants/color';
import { paddingItem, marginItem } from '@constants/staticVariable';
import { mediaQuery } from '../../utils';

// ref : https://emotion.sh/docs/typescript

// const Styled = css({
//     boxSizing: 'border-box',
//     width: 300,
//     height: 200
// });

export const Styled = styled('div')`
  label: homepage;
  min-height: 100vh;
`;

export const Container = styled('div')`
  width: 78%;
  height: auto;
  display: flex;
  flex-wrap: wrap;
  margin: 0 11%;
`;

export const TextTopic = styled.h1`
  maring-bottom: 15px;
`;

export const TextTitle = styled.h2`
  color: ${color['text-color']};
  margin: 50px ${marginItem}px 15px;
  padding: ${paddingItem}px;
`;

export const Grid = styled('div')`
  display: flex;
  width: 50%;
  display: flex;

  &.column {
    flex-direction: column;
    margin: 0;
  }

  &.row {
    flex-direction: row;

    ${mediaQuery(
      'md',
      `
      flex-wrap: wrap;
    `
    )};
  }

  &.fullwidth {
    width: 100%;
  }

  ${mediaQuery(
    'md',
    `
    width: 100vw;
  `
  )};
`;

export const Item = styled('div')`
  width: 100%;
  padding: ${paddingItem}px;
  margin: ${marginItem}px;
  position: relative;
  min-height: 180px;

  &.normal {
    width: calc((100% - ${marginItem * 6}px) / 3);

    ${mediaQuery('md', `
      width: 100%;
    `)}
  }
`;

export const SectionContainer = styled('div')`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;

export const ControllerDiv = styled('div')`
  width: 100%;
  margin: 15px ${marginItem}px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: initial;

  ${mediaQuery('md', `
    flex-direction: column;
    align-items: center;
  `)}
`;

export const BookmarkSection = styled('div')`
  display: flex;
  flex-direction: row;
  margin-top: 25px;
  align-items: initial;

  ${mediaQuery('lg', `
    margin-top: 10px;
  `)}

  ${mediaQuery('md', `
    flex-direction: column;
    align-items: center;
  `)}
`;

export default {};
