/* eslint-disable max-lines */
/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-no-undef */
import React, { useState, useEffect } from 'react';
import anime from 'animejs';
import { Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom'; // RouteComponentProps
import { get, map, size, slice, sortBy, reverse } from 'lodash';
import newsData from '@constants/newsData';
import sportData from '@constants/sportData';
import lifeData from '@constants/lifeData';
import cultureData from '@constants/cultureData';

import color from '@constants/color';

import { 
  // getFormattingData, 
  sortNewest, 
  sortOldest, 
  findUsingImage,
} from '../../utils';

// component
import { 
  NewsCard, 
  Bookmark, 
  Dropdown, 
  Loading, 
  ErrorMessage 
} from '@components/index';

import { 
  Styled, 
  Container, 
  TextTopic, 
  TextTitle, 
  Grid,
  Item,
  SectionContainer,
  ControllerDiv,
  BookmarkSection
} from './HomePage.style';

interface HomeProps {
  children?: React.ReactNode;
}

const HomePage: React.FC<HomeProps> = () => {
  const [dataLife, setDataLife] = useState(lifeData);
  const [dataCulture, setDataCulture] = useState(cultureData);
  const [dataSport, setDataSport] = useState(sportData);
  const [dataNews, setDataNews] = useState(newsData);
  const [isLoad, setIsLoad] = useState(false);
  const ifConnected = window.navigator.onLine;

  const history = useHistory();

  // go to page
  const goToSavedArtical = () => {
    history.push('/savearticle');
  };

  const handleSort = (sortWith: string) => {
    if (sortWith === 'newsest'){
      setDataLife(sortNewest(dataLife));
      setDataCulture(sortNewest(dataCulture));
      setDataSport(sortNewest(dataSport));
      setDataNews(sortNewest(dataNews));
    } else {
      setDataLife(sortOldest(dataLife));
      setDataCulture(sortOldest(dataCulture));
      setDataSport(sortOldest(dataSport));
      setDataNews(sortOldest(dataNews));
    }
  };

  // refresh content
  // const getData = async () => {
  //   const allData = await getRefreshContent();
  //   const { news, life, sport, culture } = allData;
  //   setContent(allData);
  //   setDataNews(news);
  //   setDataLife(life);
  //   setDataSport(sport);
  //   setDataCulture(culture);
  // };

  // render and handle function
  const renderSectionHighlight = () => {
    const data = slice(dataNews, 0, 5);
    const [ dataP1, dataP2, dataP3, dataP4, dataP5 ] = data;
    return (
      <>
        <Grid>
          <Item className="card-anime">
            <NewsCard 
              path={`/article/home/${get(dataP1, 'id')}`}
              className="highlight"
              title={get(dataP1, 'title')}
              pathImg={findUsingImage(dataP1)}
              lineColor={get(color, 'line-green')}
            />
          </Item>
        </Grid>
        <Grid className="row">
          <Grid className="column fullwidth">
            <Grid className="row fullwidth">
              <Item className="card-anime">
                <NewsCard
                  className="sub-higlight1"
                  path={`/article/home/${get(dataP2, 'id')}`}
                  title={get(dataP2, 'title')}
                  pathImg={findUsingImage(dataP2)}
                  lineColor={get(color, 'line-red')}
                />
              </Item>
              <Item className="card-anime">
                <NewsCard 
                  className="sub-higlight2"
                  path={`/article/home/${get(dataP3, 'id')}`}
                  title={get(dataP3, 'title')}
                  pathImg={findUsingImage(dataP3)}
                  lineColor={get(color, 'line-yellow')}
                />
              </Item>
            </Grid>
            <Grid className="row fullwidth">
              <Item className="card-anime">
                <NewsCard 
                  className="sub-higlight"
                  path={`/article/home/${get(dataP4, 'id')}`}
                  title={get(dataP4, 'title')}
                  pathImg={findUsingImage(dataP4)}
                  lineColor={get(color, 'line-lightblue')}
                />
              </Item>
              <Item className="card-anime">
                <NewsCard 
                  className="sub-higlight"
                  path={`/article/home/${get(dataP5, 'id')}`}
                  title={get(dataP5, 'title')}
                  pathImg={findUsingImage(dataP5)}
                  lineColor={get(color, 'line-green')}
                />
              </Item>
            </Grid>
          </Grid>
        </Grid>
      </>
    );
  };

  const renderSection = (type = 'News') => {
    const dataSection = (type === 'News') ? slice(dataNews, 5, size(dataNews)) : eval(`data${type}`);

    const allData = map(dataSection, (data, index) => {
      return (
        <Item className="normal card-anime" key={`card-${type}-${index}`}>
          <NewsCard
            className="normal"
            title={get(data, 'title')}
            pathImg={findUsingImage(data)}
            lineColor={get(color, 'line-red')}
            path={`/article/home/${get(data, 'id')}`}
            body={get(data, 'bodyExample')}
          />
        </Item>
      );
    });

    return (
      <>
        {allData}
      </>
    );
  };

  // useEffect section
    useEffect(() => {
    //   getData();
      anime({
        targets: '.card-anime',
        translateY: [50, 0],
        opacity: [0 , 1],
        ease: 'linear',
        delay: anime.stagger(50)
      });
    }, []);

  useEffect(() => {

    if (size(dataLife) > 0 || size(dataCulture) > 0 || size(dataNews) > 0 || size(dataSport) > 0) {
      setIsLoad(true);
    }

  }, [dataLife, dataCulture, dataSport, dataNews]);

  return (
    <Styled>
      <Container>
      {size(dataNews) > 0 && ifConnected && (
        <>
          <Grid className="row fullwidth mrt-20">
            <ControllerDiv>
              <TextTopic className="font-serif">Top Stories</TextTopic>
              <BookmarkSection>
                <Bookmark onClick={goToSavedArtical} />
                <Dropdown onClick={handleSort} />
              </BookmarkSection>
            </ControllerDiv>
          </Grid>
          {renderSectionHighlight()}
          {renderSection()}
        </>
      )}
      {size(dataSport) > 0 && (
        <Grid className="column fullwidth">
          <TextTitle className="font-serif">Sports</TextTitle>
          <SectionContainer>
            {renderSection('Sport')}
          </SectionContainer>
        </Grid>
      )}
      {size(dataCulture) > 0 && (
        <Grid className="column fullwidth">
          <TextTitle className="font-serif">Culture</TextTitle>
          <SectionContainer>
            {renderSection('Culture')}
          </SectionContainer>
        </Grid>
      )}
      {size(dataCulture) > 0 && (
        <Grid className="column fullwidth">
          <TextTitle className="font-serif">Life and Style</TextTitle>
          <SectionContainer>
            {renderSection('Life')}
          </SectionContainer>
        </Grid>
      )}
    </Container>
    {!isLoad && ifConnected && (
      <div><Loading /></div>
    )}
    {!ifConnected && (
      // eslint-disable-next-line react/jsx-no-undef
      <ErrorMessage text="Network is down. Please, check the network." />
    )}
  </Styled>
  );
};

export default HomePage;
