import * as React from 'react';
import renderWithTheme from '@helpers/tests/renderWithTheme';
import NotFoundPage from './NotFoundPage';

describe('NotFoundPage', () => {
  const mockedProps = {};

  it('should renders Page Not Found', () => {
    const { getByText } = renderWithTheme(<NotFoundPage {...mockedProps} />);
    expect(getByText('Page Not Found')).toBeInTheDocument();
  });
});
