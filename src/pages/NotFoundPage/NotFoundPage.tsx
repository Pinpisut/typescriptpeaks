import * as React from 'react';
import { errorHeaderStyle } from './NotFoundPage.style';

const NotFoundPage: React.FC = () => {
  return <h1 css={errorHeaderStyle}>Page Not Found</h1>;
};

export default NotFoundPage;
