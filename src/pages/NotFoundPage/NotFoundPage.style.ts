import { css } from '@emotion/react';
import { StyleFunction } from '@styles/theme';

export const errorHeaderStyle: StyleFunction = (theme) => css`
  color: ${theme.palette.error.dark};
  text-align: center;
  padding: ${theme.spacing(2)};
`;
