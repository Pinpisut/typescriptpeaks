import styled from '@emotion/styled';

import color from '@constants/color';
import { marginItem } from '@constants/staticVariable';
import { mediaQuery } from '../../utils';

export const Styled = styled.div`
  label: articlepage;
  min-height: 100vh;
`;

export const Container = styled.div`
  width: 78%;
  height: auto;
  display: flex;
  flex-wrap: wrap;
  margin: 40px 11% 0;
  justify-content: center;
`;

export const ContainerDetail = styled.div`
  width: 100%;
  margin: 0 ${marginItem}px;
`;

export const TextDate = styled.h4`
  margin: 20px 0;
  color: ${color['text-color']};
`;

export const TextTitle = styled.h2`
  margin: 0 0 10px;
  line-height: 1;
  color: ${color['text-color']};
`;

export const TextHeadline = styled.div`
  font-size: 30px;
  margin: 0 0 10px;
  color: ${color['text-color']};
`;

export const TextDetail = styled.h4`
  letter-spacing: 0.5px;
  color: ${color['text-color']};
`;

export const TextCaption = styled.h5`
  margin: 10px 0;
  color: ${color['text-color-thrid']};
`;

export const GridContainer = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;

  ${mediaQuery(
    'lg',
    `
    flex-direction: column;
  `
  )};
`;

type GridProp = {
    fullwidth?: boolean,
    size: number,
    order?: number
}

export const Grid = styled.div<GridProp>`
  width: ${props => props.fullwidth ? '100%' : (props.size) ? `${props.size}%` : '50%'};
  margin: 0px;
  display: flex;
  flex-direction: column;
  
  ${props => {
    return `
      ${mediaQuery('lg', `
      width: 100%;
        order: ${props.order}
      `)}
    `;
  }}
`;

export const DataContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  ${mediaQuery(
    'lg',
    `
    flex-direction: column;
  `
  )};
`;

export const TimeSection = styled.div`
  display: flex;
  flex-direction: column;
  margin: 30px 0 0 0;
`;

export const ImageContainer = styled.div`
  display: flex;
  flex-direction: column;

  ${mediaQuery(
    'lg',
    `
    align-items: center;
    text-align: center;
  `
  )};
`;

export const ImageDiv = styled.img`
  display: flex;
  width: 100%;
  max-height: 350px;
  object-fit: cover;
`;

export const LineSeparateDiv = styled.div`
  width: 100%;
  margin: 10px 0 20px;
  border: 0.2px solid ${color['line-color']};
`;

export const SneckBarDiv = styled.div`
  width: 70%;
  height: 30px;
  background: ${color['primary-color']};
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 0 20px;

  ${mediaQuery(
    'md',
    `
    width: 100%;
  `
  )};
`;

export const TextSneckBar = styled.h4`
  font-size: 15px;
  color: ${color['text-color-secondary']};
`;