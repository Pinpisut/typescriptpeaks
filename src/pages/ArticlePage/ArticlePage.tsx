/* eslint-disable max-lines */
/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
import React, { useState, useEffect } from 'react';
import dayjs from 'dayjs';
import { get, filter, split, map, find, remove, size, join, indexOf } from 'lodash';
import { useParams } from 'react-router-dom';

import newsData from '@constants/newsData';
import sportData from '@constants/sportData';
import lifeData from '@constants/lifeData';
import cultureData from '@constants/cultureData';

import { findUsingImage } from '../../utils';

//component
import { Bookmark, ErrorMessage, Loading } from '@components/index';

import {
  Styled,
  Container,
  SneckBarDiv,
  TextSneckBar,
  ContainerDetail,
  TimeSection,
  TextDate,
  GridContainer,
  Grid,
  TextTitle,
  TextHeadline,
  LineSeparateDiv,
  DataContainer,
  TextDetail,
  ImageContainer,
  ImageDiv,
  TextCaption
} from './ArticlePage.style';

type objectItemOption = {
  [key: string]: string
};

type paramsOption = {
  id:string,
  frompage: string
}

const ArticlePage: React.FC = () => {
  const { id, frompage } = useParams<paramsOption>();
  const [dataArticle, setDataArticle] = useState({});
  const [isLoad, setIsLoad] = useState(false);
  const [isBookmark, setIsBookmark] = useState(false);
  const [showSnackBar, setShowSnackBar] = useState(false);
  const [isClickBookmark, setIsClickBookmark] = useState(false);
  const ifConnected = window.navigator.onLine;
  const allData = [...newsData, ...sportData, ...lifeData, ...cultureData];

  // get content
  const findContentByData = (allData:Array<objectItemOption>, id: string) => {
    const findData = filter(allData, data => get(data, 'id') === id);
    return get(findData, '0', {});
  };

  const getData = async () => {
    const data = findContentByData(allData, id);
    setDataArticle(data);
    setIsLoad(true);
  };

  const getDataFromLocalStorage = () => {
    const dataInLocal = localStorage.getItem('article-book');
    let arrayLocalStorage = {};
    if (dataInLocal === null) {
      setDataArticle(arrayLocalStorage);
    } else {
      const arrayKey = split(dataInLocal, ',');
      const dataMap = map(arrayKey, (strKey) => {
        const getObjectFromLocalStorage = localStorage.getItem(strKey);
        const jsonObjectFromLocalStorage = JSON.parse(getObjectFromLocalStorage || '{}');
        return jsonObjectFromLocalStorage;
      });
      arrayLocalStorage = find(dataMap, data => get(data, 'id') === id);
    }

    setDataArticle(arrayLocalStorage);
    setIsLoad(true);
  };

  // render and handle function
  const setBookmark = () => {
    
    if (isBookmark) {
      // remove bookmark
      // set value of article-book
      const dataInLocal = localStorage.getItem('article-book');
      const arrLocal = split(dataInLocal, ',');
      const removeTheId = remove(arrLocal, item => {
        return item !== id;
      });

      if (size(removeTheId) === 0) {
        localStorage.removeItem('article-book');
      } else {
        const newKey = join(removeTheId, ',');
        localStorage.setItem('article-book', newKey);
      }

      // remove obj
      localStorage.removeItem(id);
    } else {
      // add bookmark
      const dataInLocal = localStorage.getItem('article-book');
      if (dataInLocal !== null && dataInLocal !== '') {
        const newKey = `${dataInLocal},${id}`;
        localStorage.setItem('article-book', newKey);
      } else {
        localStorage.setItem('article-book', id);
      }
      
      localStorage.setItem(id, JSON.stringify(dataArticle));
    }

    setIsBookmark(!isBookmark);
    setIsClickBookmark(true);
  };

  const checkBookMark = () => {
    const dataInLocal = localStorage.getItem('article-book');
    const arrLocal = split(dataInLocal, ',');
    const isBool = indexOf(arrLocal, id) >= 0;
    setIsBookmark(isBool);
  };

  const clearSneckBar = () => {
    setTimeout((() => {
      setShowSnackBar(false);
    }), 800);
  };

  const findContentById = (id:string) => {
    const findData = filter(allData, data => get(data, 'id') === id);
    return get(findData, '0', {});
  };

  // useEffect section
  useEffect(() => {
    const dataById = findContentById(id);
    if (size(dataById) > 0) {
      // found data match
      setIsLoad(true);
      checkBookMark();
      setDataArticle(dataById);

    } else {
      // found data not match
      // from saved article
      if (frompage === 'savedArticle') {
        checkBookMark();
        getDataFromLocalStorage();
      } else {
        checkBookMark();
        getData();
      }
    }
  }, []);

  useEffect(() => {
    if (isClickBookmark) {
      setShowSnackBar(true);
      clearSneckBar();
    }
  }, [isBookmark, isClickBookmark]);

  return (
    <Styled>
      {isLoad && ifConnected && dataArticle !== {} && (
        <Container>
          {showSnackBar && isClickBookmark && (
            <SneckBarDiv>
              <TextSneckBar>{(!isBookmark)? 'Saved article has been removed.' : 'The article is saved.'}</TextSneckBar>
            </SneckBarDiv>
          )}
          <ContainerDetail>
            <TimeSection>
              <Bookmark key={`bookmark-${isBookmark}`} nowBook={isBookmark} nowArticle onClick={setBookmark} />
              <TextDate className="font-opensan-light">{dayjs(get(dataArticle, 'date')).format('ddd D MMM YYYY H.mm')}</TextDate>
            </TimeSection>
            <GridContainer>
              <Grid fullwidth={get(dataArticle, 'defaultImage') === 'none'} size={55}>
                <TextTitle className="font-serif">{get(dataArticle, 'title')}</TextTitle>
                {get(dataArticle, 'headline') && (
                  <TextHeadline className="font-serif">
                    {get(dataArticle, 'headline')}
                  </TextHeadline>
                )}
                <LineSeparateDiv />
              </Grid>
              <DataContainer>
                <Grid order={2} fullwidth={get(dataArticle, 'defaultImage') === 'none'} size={55}>
                  <TextDetail className="font-opensan-light" dangerouslySetInnerHTML={{__html: get(dataArticle, 'bodyText')}} />
                </Grid>
                {get(dataArticle, 'defaultImage') !== 'none' && (
                  <Grid order={1} size={40}>
                    <ImageContainer>
                      <ImageDiv src={findUsingImage(dataArticle)} alt ="image-content" />
                      <TextCaption className="font-opensan-light">{get(dataArticle, 'caption')}</TextCaption>
                    </ImageContainer>
                  </Grid>
                )}
              </DataContainer>
            </GridContainer>
          </ContainerDetail>
        </Container>
      )}
      {isLoad && ifConnected && dataArticle === {} && (
        <ErrorMessage text="News is out of date." />
      )}
      {!isLoad && ifConnected && (
        <div><Loading /></div>
      )}
      {!ifConnected && (
        <ErrorMessage text="Network is down. Please, check the network." />
      )}
    </Styled>
  );
};

export default ArticlePage;