/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
import React, { useState, useEffect } from 'react';
import anime from 'animejs';
import { split, size, map, get } from 'lodash';

import color from '@constants/color';
// component
import { 
  NewsCard, 
  Dropdown, 
  ErrorMessage, 
  Loading 
} from '@components/index';

import { 
  sortNewest, 
  sortOldest,
} from '../../utils';

import { 
  Styled, 
  Container,
  Grid,
  Item,
  SectionContainer,
  ControllerDiv,
  BookmarkSection
} from './SavedArticlePage.style';

type sortItemOption = {
  [key: string]: string
};

const SavedArticlePage: React.FC = () => {

  const [savedData, setSavedData] = useState<Array<sortItemOption>>([]);
  const [isLoad, setIsLoad] = useState(false);
  const ifConnected = window.navigator.onLine;

  // get content
  const getData = () => {
    const dataInLocal = localStorage.getItem('article-book');
    let arrayLocalStorage:Array<sortItemOption> = [];
    if (dataInLocal === null) {
      setSavedData(arrayLocalStorage);
    } else {
      const arrayKey = split(dataInLocal, ',');
      const dataMap = map(arrayKey, (strKey) => {
        const getObjectFromLocalStorage = localStorage.getItem(strKey);
        const jsonObjectFromLocalStorage = JSON.parse(getObjectFromLocalStorage || '{}');
        return jsonObjectFromLocalStorage;
      });
      arrayLocalStorage = sortNewest(dataMap);
    }

    setSavedData(arrayLocalStorage);
    setTimeout(() => {
      setIsLoad(true);
    }, 500);
  };

  // render and handle function
  const handleSort = (sortWith:string) => {
    if (sortWith === 'newsest'){
      setSavedData(sortNewest(savedData));
    } else {
      setSavedData(sortOldest(savedData));
    }
  };

  const renderItem = () => {
    const allItem =  map(savedData, (data, index) => {
      return (
        <Item className="normal card-anime" key={`card-saved-${index}`}>
          <NewsCard
            key={`newscard-saved-${index}`}
            className="normal"
            title={get(data, 'title')}
            pathImg={get(data, 'imageThumb')}
            lineColor={get(color, 'line-red')}
            path={`/article/savedArticle/${get(data, 'id')}`}
          />
        </Item>
      );
    });

    return (
      <>
        {allItem}
      </>
    );
  };

  // useEffect section
  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {

    if (size(savedData) > 0) {
      anime({
        targets: '.card-anime',
        translateY: [50, 0],
        opacity: [0 , 1],
        ease: 'linear',
        delay: anime.stagger(50)
      });
    }

  }, [savedData]);

  return (
    <Styled>
      {isLoad && ifConnected && (
        <Container>
          <Grid className="row fullwidth mrt-20">
            <ControllerDiv>
              <h1 className="font-serif">All Bookmark</h1>
              <BookmarkSection>
                <Dropdown onClick={handleSort} />
              </BookmarkSection>
            </ControllerDiv>
          </Grid>
          {size(savedData) > 0 && (
            <Grid className="column fullwidth">
              <SectionContainer>
                {renderItem()}
              </SectionContainer>
            </Grid>
          )}
        </Container>
      )}
      {isLoad && ifConnected && size(savedData) === 0 && (
        <div><ErrorMessage text="Saved article is not found." /></div>
      )}
      {!isLoad && ifConnected && (
        <div><Loading /></div>
      )}
      {!ifConnected && (
        <ErrorMessage text="Network is down. Please, check the network." />
      )}
  </Styled>
  );
};

export default SavedArticlePage;
