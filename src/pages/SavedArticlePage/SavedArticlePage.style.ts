import styled from '@emotion/styled';

import { paddingItem, marginItem } from '@constants/staticVariable';
import { mediaQuery } from '../../utils';

export const Styled = styled.div`
  label: savedarticlepage;
  min-height: 100vh;
`;

export const Container = styled.div`
  width: 78%;
  height: auto;
  display: flex;
  flex-wrap: wrap;
  margin: 0 11%;
`;

export const Grid = styled.div`
  display: flex;
  width: 100%;
  display: flex;

  flex-direction: row;

  ${mediaQuery(
    'md',
    `
    flex-wrap: wrap;
  `
  )};
`;

export const ControllerDiv = styled.div`
  width: 100%;
  margin: 20px ${marginItem}px 15px;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: initial;

  ${mediaQuery('md', `
    flex-direction: column;
    align-items: center;
  `)}
`;

export const BookmarkSection = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 20px;
  align-items: initial;

  ${mediaQuery('md', `
    flex-direction: column;
    align-items: center;
    margin-top: 0px;
  `)}
`;

export const SectionContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`;

export const Item = styled.div`
  width: 100%;
  padding: ${paddingItem}px;
  margin: ${marginItem}px;
  background: yellow;
  position: relative;
  min-height: 180px;

  &.normal {
    width: calc((100% - ${marginItem * 6}px) / 3);

    ${mediaQuery('md', `
      width: 100%;
    `)}
  }
`;
