/* eslint-disable max-lines */
/* eslint-disable max-lines-per-function */
/* eslint-disable complexity */
import React, { useState, useEffect } from 'react';
import { get, size, slice, map } from 'lodash';
import { useParams, useHistory } from 'react-router-dom';

import newsData from '@constants/newsData';
import sportData from '@constants/sportData';
import lifeData from '@constants/lifeData';
import cultureData from '@constants/cultureData';

import color from '@constants/color';
import { numberPerPage } from '@constants/staticVariable';

//component
import { 
  NewsCard,
  Bookmark,
  Dropdown,
  ErrorMessage,
  Loading
} from '@components/index';

import { 
  sortNewest, 
  sortOldest,
  searchByKey,
  findUsingImage
} from '../../utils';

import {
  Styled,
  Container,
  Grid,
  ControllerDiv,
  BookmarkSection,
  SectionContainer,
  Item
} from './SearchPage.style';

type objectItemOption = {
  [keyItem: string]: string
};

type paramsOption = {
  key:string
}

const SearchPage: React.FC = () => {
  const { key } = useParams<paramsOption>() || undefined;
  const [nowKey, setNowKey] = useState();
  const [listItems, setListItems] = useState<Array<objectItemOption>>([]);
  const [isFetching, setIsFetching] = useState(false);
  const [searchData, setSearchData] = useState<Array<objectItemOption>>([]);
  const [isLoad, setIsLoad] = useState(false);
  const [currentPaginate, setCurrentPaginate] = useState(0);
  const [isFound, setIsFound] = useState(false);
  const [isNewest, setIsNewest] = useState(true);
  const ifConnected = window.navigator.onLine;
  const allData = [...newsData, ...sportData, ...lifeData, ...cultureData];

  const history = useHistory();

  const getData = () => {
    setIsLoad(true);
  };

  // render and handle function
  const handleScroll = () => {
    const nowScrollScreen = window.innerHeight + document.documentElement.scrollTop;
    const isNotLast = (nowScrollScreen !== document.documentElement.offsetHeight);
    if (isNotLast) return;
    setIsFetching(true);
  };

  const fetchMoreListItems = () => {
    const numberPage = currentPaginate + 1;
    const numberElement = numberPerPage * numberPage;
    const allSize = size(searchData);
    let nowItem:Array<objectItemOption> = [];
    if (size(searchData) !== size(listItems)){
      if (numberElement < allSize) {
        nowItem = slice(searchData, 0, numberPerPage * numberPage);
      } else {
        nowItem = searchData;
      }
  
      setTimeout(() => {
        setListItems(nowItem);
        setCurrentPaginate(numberPage);
        setIsFetching(false);
      }, 1000);
    }
  };

  const renderItem = () => {
    const dataMap = map(listItems, (data, index) => (
      <Item key={`search-item-${index}`} className="normal">
        <NewsCard
          key={`newscard-item-${get(data, 'id')}`}
          className="normal"
          title={get(data, 'title')}
          pathImg={findUsingImage(data)}
          lineColor={get(color, 'line-red')}
          path={`/article/search/${get(data, 'id')}`}
        />
      </Item>
      
    ));

    return (
      <>
        {dataMap}
      </>
    );
  };

  const findDataForUse = (data:Array<objectItemOption>) => {
    setTimeout(() => {
      const onlySomeElement = slice(data, 0, numberPerPage);
      setListItems(onlySomeElement);
      setCurrentPaginate(1);
    }, 1000);
  };

  const handleSort = (sortWith: string) => {
    if (sortWith === 'newsest'){
      const newItem = sortNewest(listItems);
      const newSearchData:Array<objectItemOption> = sortNewest(searchData);
      setSearchData(newSearchData);
      setListItems(newItem);
      setIsNewest(true);
    } else {
      const oldItem = sortOldest(listItems);
      const oldSearchData = sortOldest(searchData);
      setSearchData(oldSearchData);
      setListItems(oldItem);
      setIsNewest(false);
    }
  };

  // route to other
  const goToSavedArtical = () => {
    history.push('/savearticle');
  };

  // useEffect section
  useEffect(() => {

    getData();

  }, []);

  useEffect(() => {

    if (size(allData) > 0) {
      const data = searchByKey(allData, key);
      const newestData = (isNewest) ? sortNewest(data) : sortOldest(data);
      const isFoundData = (size(newestData) !== 0);
      setIsFound(isFoundData);
      setIsFetching(false);
      if (isFoundData) {
        setSearchData(newestData);
        findDataForUse(newestData);
      }
    }

    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [nowKey]);

  useEffect(() => {
    if (!isFetching) return;
    fetchMoreListItems();
  }, [isFetching]);

  useEffect(() => {
    setNowKey(key);
  }, [key]);

  return (
    <Styled>
      {isLoad && ifConnected && (
        <Container>
          <Grid className="row fullwidth mrt-20">
            <ControllerDiv>
              <h1 className="font-serif">Search result</h1>
              <BookmarkSection>
                <Bookmark nowBook={true} onClick={goToSavedArtical} />
                <Dropdown onClick={handleSort} />
              </BookmarkSection>
            </ControllerDiv>
          </Grid>
          {isFound && (
            <SectionContainer>
              {renderItem()}
            </SectionContainer>
          )}
          {isFetching && size(searchData) !== size(listItems) && (
            <ErrorMessage text="Fetching more list items..." />
          )}
          {!isFound && (
            <ErrorMessage text="Please search with another keyword." />
          )}
        </Container>
      )}
      {!isLoad && ifConnected && (
        <div><Loading /></div>
      )}
      {!ifConnected && (
        <ErrorMessage text="Network is down. Please, check the network." />
      )}
  </Styled>
  );
};

export default SearchPage;