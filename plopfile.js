const generateComponent = {
  type: 'add',
  path: 'src/components/{{pascalCase name}}/{{pascalCase name}}.tsx',
  templateFile: 'plop-templates/Component.tsx.hbs',
};
const generateStyle = {
  type: 'add',
  path: 'src/components/{{pascalCase name}}/{{pascalCase name}}.style.ts',
  templateFile: 'plop-templates/Component.style.ts.hbs',
};
const generateTest = {
  type: 'add',
  path: 'src/components/{{pascalCase name}}/{{pascalCase name}}.test.tsx',
  templateFile: 'plop-templates/Component.test.tsx.hbs',
};
const generateIndex = {
  type: 'add',
  path: 'src/components/{{pascalCase name}}/index.ts',
  templateFile: 'plop-templates/index.ts.hbs',
};

module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
      },
    ],
    actions: [generateComponent, generateStyle, generateTest, generateIndex],
  });
};
