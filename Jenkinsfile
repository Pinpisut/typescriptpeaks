#!groovy

projectName = 'sample-project'
slackChannel = 'devops_sample_project_dev'

appConfigs = [
  develop: [
    environmentName: 'development',
    credentials: '',
    bucket: '',
    distributionId: '',
  ],
  uat: [
    environmentName: 'uat',
    credentials: '',
    bucket: '',
    distributionId: '',
  ],
  master: [
    environmentName: 'production',
    credentials: '',
    bucket: '',
    distributionId: '',
  ]
]

node('master') {
  try {

    if (env.BRANCH_NAME.contains('/')) {
      branchName = env.BRANCH_NAME.replaceFirst('.+/', '')
    } else {
      branchName = env.BRANCH_NAME
    }

    if (appConfigs[branchName] != null){
      environmentName = appConfigs[branchName].environmentName
    } else {
      environmentName = "local"
    }

    properties([buildDiscarder(logRotator(numToKeepStr: '2'))])

    stage('Clone repository') {
      checkout scm
    }

    stage('Build') {
        dockerApiImage = docker.build("${projectName}:${branchName}_${env.BUILD_ID}",
          """ -f Dockerfile.cd \
              --build-arg Environment=${environmentName} \
              --build-arg BuildNumber=${env.BUILD_NUMBER} .
          """)
    }

    stage('Deploy') {
      milestone()

      if (appConfigs[branchName] != null) {
        def credentials = appConfigs[branchName].credentials
        def bucketName = appConfigs[branchName].bucket
        def distributionId = appConfigs[branchName].distributionId

        milestone()

        sh 'mkdir ./build'
        dockerApiImage.withRun("-v ${WORKSPACE}/build:/mnt") {}

        withAWS(credentials: credentials, region: 'ap-southeast-1') {
            s3Delete(bucket: bucketName, path: '')
            s3Upload(file: 'build', bucket: bucketName, path: '')
            cfInvalidate(distribution: distributionId, paths: ['/*'], waitForCompletion: true)
        }
      }
    }

    stage('Clean') {
      sh "docker image rmi -f ${projectName}:${branchName}_${env.BUILD_ID}"
    }

    currentBuild.result = 'SUCCESS'
  } catch(e) {
    throw e

  } finally {
    // send result
    def currentResult = currentBuild.result == 'SUCCESS' ? '👍 success': '👎 failure'

    // send message to slack
    withCredentials([string(credentialsId: 'slack-giorgio-token', variable: 'TOKEN')]) {
      docker.image('mikewright/slack-client:latest').run("-e SLACK_TOKEN=$TOKEN -e SLACK_CHANNEL=##${slackChannel} --rm", "\"${env.JOB_NAME}/${env.BRANCH_NAME} - Build#${env.BUILD_NUMBER}\n Status: ${currentResult}\n ${env.BUILD_URL}\"")
    }
  }
}