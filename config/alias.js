module.exports = {
  '@api': './src/api',
  '@assets': './src/assets',
  '@components': './src/components',
  '@constants': './src/constants',
  '@providers': './src/providers',
  '@hooks': './src/hooks',
  '@pages': './src/pages',
  '@styles': './src/styles',
  '@helpers': './src/helpers',
  '@routes': './src/routes',
  '@layouts': './src/layouts',
  '@utils': './src/utils',
};
