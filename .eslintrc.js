module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  plugins: ['@typescript-eslint', 'react'],
  extends: ['plugin:react/recommended', 'plugin:@typescript-eslint/recommended', 'plugin:react-hooks/recommended'],
  rules: {
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    'react/prop-types': 'off',
    'no-console': 'error',
    '@typescript-eslint/no-empty-interface': 'warn',
    '@typescript-eslint/no-explicit-any': 'warn',
    // Rules regarding minimise code complexity
    'no-param-reassign': ['error'],
    'max-lines': ['warn', { max: 200, skipBlankLines: true }],
    'max-lines-per-function': ['warn', { max: 25, skipBlankLines: true }],
    complexity: ['warn', { max: 5 }],
    'max-nested-callbacks': ['warn', { max: 2 }],
    'max-depth': ['warn', { max: 3 }],
    'max-params': ['warn', { max: 2 }],
  },
};
