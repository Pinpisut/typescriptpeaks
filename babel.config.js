const alias = require('./config/alias');

module.exports = {
  extends: '@snowpack/app-scripts-react/babel.config.json',
  presets: [
    [
      '@babel/preset-env',
      {
        targets: { esmodules: true },
        bugfixes: true,
      },
    ],
    '@babel/preset-typescript',
    '@babel/preset-react',
    '@emotion/babel-preset-css-prop',
  ],
  plugins: [
    [
      'module-resolver',
      {
        alias,
      },
    ],
  ],
};
